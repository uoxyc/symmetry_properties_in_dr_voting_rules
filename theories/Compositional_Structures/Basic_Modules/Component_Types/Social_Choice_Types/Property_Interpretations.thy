(*  File:       Property_Interpretations.thy
    Copyright   2024  Karlsruhe Institute of Technology (KIT)
*)
\<^marker>\<open>creator "Alicia Appelhagen, Karlsruhe Institute of Technology (KIT)"\<close>

section \<open>Result-Dependent Voting Rule Properties\<close>

theory Property_Interpretations
  imports Voting_Symmetry
          Result_Interpretations
begin

subsection \<open>Properties Dependent on the Result Type\<close>

text \<open>
  The interpretation of equivariance properties generally depends on the result type.
  For example, neutrality for social choice rules means that single winners are renamed
  when the candidates in the votes are consistently renamed. For social welfare results,
  the complete result rankings must be renamed.

  New result-type-dependent definitions for properties can be added here.
\<close>

locale result_properties = result +
  fixes \<psi>_neutr :: "('a \<Rightarrow> 'a, 'b) binary_fun" and
        \<E> :: "('a, 'v) Election"
  assumes
    act_neutr: "group_action neutrality\<^sub>\<G> UNIV \<psi>_neutr" and
    well_formed_res_neutr:
      "satisfies (\<lambda> \<E> :: ('a, 'v) Election. limit_set (alternatives_\<E> \<E>) UNIV)
                (equivar_ind_by_act (carrier neutrality\<^sub>\<G>)
                    valid_elections (\<phi>_neutr valid_elections) (set_action \<psi>_neutr))"

sublocale result_properties \<subseteq> result
  using result_axioms
  by simp

subsection \<open>Interpretations\<close>

global_interpretation social_choice_properties:
  "result_properties" "well_formed_social_choice" "limit_set_social_choice" "\<psi>_neutr\<^sub>\<c>"
  unfolding result_properties_def result_properties_axioms_def
  using wf_res_neutr_social_choice \<psi>_neutr\<^sub>\<c>_act.group_action_axioms
        social_choice_result.result_axioms
  by blast

global_interpretation social_welfare_properties:
  "result_properties" "well_formed_welfare" "limit_set_welfare" "\<psi>_neutr\<^sub>\<w>"
  unfolding result_properties_def result_properties_axioms_def
  using wf_res_neutr_social_welfare \<psi>_neutr\<^sub>\<w>_act.group_action_axioms
        social_welfare_result.result_axioms
  by blast

end